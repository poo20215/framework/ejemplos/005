<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<style>
    *{
        text-align: center;
    }
</style>
<div>
    <h1>CONCESIONARIO LEON</h1>
    
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non feugiat urna. Nunc dapibus sit amet justo eu ullamcorper. In tempus ullamcorper pellentesque. 
       Vivamus vel scelerisque arcu, a venenatis diam. Vivamus at pellentesque orci. Nullam malesuada, justo mollis posuere elementum, metus velit viverra eros, 
       sed aliquam lacus lectus ac ipsum. Aenean felis odio, semper sed cursus in, commodo nec diam. Praesent consectetur sollicitudin sem id imperdiet. Aenean mauris ante, 
       pretium vitae urna sagittis, cursus gravida diam. Vestibulum elit nisi, iaculis in pharetra eu, volutpat eu tellus. Fusce pulvinar rhoncus mauris, 
       hendrerit blandit lacus auctor ut. Aliquam a viverra nibh. Etiam ac ultrices quam.
    </p>
    
    <p>
        Sed ut porttitor leo, ac sollicitudin ipsum. Vestibulum volutpat feugiat dui a commodo. Curabitur risus lacus, faucibus eget est nec, pellentesque suscipit ligula. 
        Ut dapibus, odio eget pharetra egestas, ipsum neque viverra arcu, eget blandit nisl libero sit amet ligula. Ut congue felis non enim imperdiet varius. Nulla rhoncus volutpat nibh, 
        convallis dapibus mauris posuere vitae. Ut venenatis ligula pharetra, laoreet dui ac, hendrerit felis. Ut pretium risus purus, in interdum turpis pellentesque a. 
        Vivamus fringilla egestas finibus. Vestibulum sit amet leo urna. Nullam sed mi vel lectus dictum suscipit. Integer tincidunt risus tempor, feugiat risus at, finibus nisi. 
        Nunc sapien orci, feugiat id fermentum id, malesuada eget est. Aliquam tempus efficitur lobortis.
    </p>
    
    <p>
        Curabitur ornare aliquam massa, vel luctus enim hendrerit vitae. In hac habitasse platea dictumst. Integer libero erat, maximus ut velit non, sagittis efficitur erat. 
        Sed tincidunt lacinia eros, ac placerat urna scelerisque at. Cras at nulla congue, iaculis nibh ac, ultrices turpis. Vivamus quis fringilla metus. In hac habitasse platea dictumst. 
        Curabitur gravida a sapien vel vulputate. Cras eleifend pulvinar maximus. Fusce hendrerit metus quis est ultrices bibendum. Proin eu euismod eros. Quisque a lorem congue, 
        faucibus nisl eget, auctor magna. Quisque placerat felis nibh, sed euismod lorem ultrices non. Nunc nunc mauris, rhoncus ac dapibus ac, finibus vel odio.
    </p>
    
    <?=    Html::img('@web/img/concesionario.jpg',['width' => 750])?>
    
</div>