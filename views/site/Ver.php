<?php

use yii\widgets\DetailView;
use yii\bootstrap4\Html;

echo DetailView::widget([
    'model' => $modelo,
    'attributes' => [
        'id',
        'marca',
        'modelo',
        'precio',
        'fecha_entrada',
        'cilindrada',
        [
            'label'=>'Foto',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->foto;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ]
            ]
]);