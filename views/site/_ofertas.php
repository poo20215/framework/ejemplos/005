<?php
use yii\bootstrap4\Html;
?>

<div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><?= $model->marca ?></li>
    <li class="list-group-item"><?= $model->modelo ?></li>
    <li class="list-group-item"><?= $model->precio ?></li>
    <li class="list-group-item"><?= Html::a('Ver mas...', ['site/ver', 'id' => $model->id], ['class' => 'profile-link']) ?></li>
    
  </ul>
</div>

