<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\Coches;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCoches()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Coches::find()]);
        return $this->render("vistaCoches",["dataProvider" => $dataProvider]);
    }
    
    public function actionOfertas()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Coches::find()->where(['oferta'=>1])]);
        return $this->render("vistaOfertas",["dataProvider" => $dataProvider]);
    }
    
    public function actionVer($id)
    {
        //$modelo=Coches::find()->where("id=$id")->one();
        //return $this->render("ver",["modelo"=> $modelo]);
        
        //$modelo=Coches::find()->where(["id" => $id])->one();
        //return $this->render("ver",["modelo"=> $modelo]);
        
        $modelo= Coches::findOne($id);
        return $this->render("ver",["modelo"=> $modelo]);
    }
}
