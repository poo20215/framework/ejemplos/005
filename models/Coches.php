<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coches".
 *
 * @property int $id
 * @property string|null $marca
 * @property string|null $modelo
 * @property int|null $precio
 * @property string|null $fecha_entrada
 * @property string|null $foto
 * @property int|null $cilindrada
 */
class Coches extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'cilindrada'], 'integer'],
            [['fecha_entrada'], 'safe'],
            [['marca', 'modelo', 'foto'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'precio' => 'Precio',
            'fecha_entrada' => 'Fecha Entrada',
            'foto' => 'Foto',
            'cilindrada' => 'Cilindrada',
        ];
    }
}
